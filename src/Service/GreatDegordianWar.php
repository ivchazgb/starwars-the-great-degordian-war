<?php
/**
 * Created by PhpStorm.
 * User: ivanhlevnjak
 * Date: 31/05/2018
 * Time: 17:28
 */

namespace App\Service;
use App\Entity\Fleets\Fleet;

class GreatDegordianWar
{
    public function fight($sithFleet, $republicFleet)
    {


        while ($sithFleet->isAlive() && $republicFleet->isAlive()) {

            $republicSolider = $republicFleet->getRandomSolider();
            $sithSolider = $sithFleet->getRandomSolider();

            $republicSolider->setDefence($sithSolider->attack($republicSolider->getDefence()));
            $sithSolider->setDefence($republicSolider->attack($sithSolider->getDefence()));

            //check if solders are alive
            if ($republicSolider->isDead()) {
                dump($republicSolider);
                $republicFleet->burySolider($republicSolider);
            }

            if ($sithSolider->isDead()) {
                dump($sithSolider);
                $sithFleet->burySolider($sithSolider);
            }

        }

        return $this->checkForAWinner($sithFleet, $republicFleet);



    }


    public function checkForAWinner($sithFleet, $republicFleet)
    {
        if (count($sithFleet->soliders) == 0) {
            return 'Republic';
        } else if (count($republicFleet->soliders)== 0) {
            return 'Sith';
        }
    }



}