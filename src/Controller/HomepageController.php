<?php

namespace App\Controller;

use App\Entity\Fleets\RepublicFleet;
use App\Entity\Fleets\SithFleet;
use App\Entity\SithLord;
use App\Entity\SithWarrior;
use App\Entity\StormTrooper;
use App\Service\GreatDegordianWar;
use App\Service\RepublicArmyGenerator;
use App\Service\SithArmyGenerator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class HomepageController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function index(Request $request, GreatDegordianWar $greatDegordianWar)
    {
        //gets army1 from request
        $sithNumber = $request->get('army1');

        //gets army2 from request
        $republicNumber = $request->get('army2');

        if (!isset($sithNumber) || !isset($republicNumber)){
            return $this->render('homepage/index.html.twig', [
                'error' => 'Army 1 and 2 must have numbers!',
            ]);
        }

        $sithFleet = new SithFleet();
        $sithFleet->makeNewFleet($sithNumber);

        $republicFleet = new RepublicFleet();
        $republicFleet->makeNewFleet($republicNumber);

        $winner = $greatDegordianWar->fight($sithFleet, $republicFleet);

        return $this->render('homepage/index.html.twig', [
            'winner' => $winner,
        ]);
    }

}
