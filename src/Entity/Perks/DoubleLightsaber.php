<?php
/**
 * Created by PhpStorm.
 * User: ivanhlevnjak
 * Date: 31/05/2018
 * Time: 18:38
 */

namespace App\Entity\Perks;


class DoubleLightsaber
{
    private $bonusDamage;
    private $bonusDefence;

    public function __construct()
    {
        $this->bonusDamage = 30;
        $this->bonusDefence = 30;
    }

    public function bonusDamage()
    {
        return $this->bonusDamage;
    }

    public function bonusDefence()
    {
        return $this->bonusDefence;
    }
}