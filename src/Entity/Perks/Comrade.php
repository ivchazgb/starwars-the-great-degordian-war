<?php
/**
 * Created by PhpStorm.
 * User: ivanhlevnjak
 * Date: 31/05/2018
 * Time: 18:39
 */

namespace App\Entity\Perks;


class Comrade
{
    private $bonusDamage;
    private $bonusDefence;

    public function __construct()
    {
        $this->bonusDamage = 20;
        $this->bonusDefence = 20;
    }

    public function bonusDamage()
    {
        return $this->bonusDamage;
    }

    public function bonusDefence()
    {
        return $this->bonusDefence;
    }
}