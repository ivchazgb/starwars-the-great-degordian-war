<?php
/**
 * Created by PhpStorm.
 * User: ivanhlevnjak
 * Date: 31/05/2018
 * Time: 18:36
 */

namespace App\Entity\Perks;


class DestroyerShip
{
    private $bonusDamage;
    private $bonusDefence;

    public function __construct()
    {
        $this->bonusDamage = 150;
        $this->bonusDefence = 300;
    }

    public function bonusDamage()
    {
        return $this->bonusDamage;
    }

    public function bonusDefence()
    {
        return $this->bonusDefence;
    }
}