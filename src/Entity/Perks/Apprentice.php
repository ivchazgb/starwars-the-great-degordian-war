<?php
/**
 * Created by PhpStorm.
 * User: ivanhlevnjak
 * Date: 31/05/2018
 * Time: 18:37
 */

namespace App\Entity\Perks;


class Apprentice
{
    private $bonusDamage;
    private $bonusDefence;

    public function __construct()
    {
        $this->bonusDamage = 50;
        $this->bonusDefence = 50;
    }

    public function bonusDamage()
    {
        return $this->bonusDamage;
    }

    public function bonusDefence()
    {
        return $this->bonusDefence;
    }
}