<?php
/**
 * Created by PhpStorm.
 * User: ivanhlevnjak
 * Date: 31/05/2018
 * Time: 18:35
 */

namespace App\Entity\Perks;


class R2D2
{
    private $bonusDamage;
    private $bonusDefence;

    public function __construct()
    {
        $this->bonusDamage = 20;
        $this->bonusDefence = 30;
    }

    public function bonusDamage()
    {
        return $this->bonusDamage;
    }

    public function bonusDefence()
    {
        return $this->bonusDefence;
    }
}