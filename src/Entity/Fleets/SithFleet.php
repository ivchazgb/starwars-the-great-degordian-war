<?php
/**
 * Created by PhpStorm.
 * User: ivanhlevnjak
 * Date: 01/06/2018
 * Time: 20:56
 */

namespace App\Entity\Fleets;

use App\Entity\SithLord;
use App\Entity\SithWarrior;
use App\Entity\StormTrooper;

class SithFleet extends Fleet
{
    public function __construct()
    {
        parent::__construct();
        $this->setName('Sith Fleet');
    }

    public function makeNewFleet(int $number): array
    {
        for ($i = 0; $i < $number; $i++) {

            $this->soliders[] = $this->getRandomEmpireClass();

        }
        return $this->soliders;
    }

    private function getRandomEmpireClass()
    {
        switch (rand(0, 2)) {
            case 0:
                $unit = new SithLord();
                break;

            case 1:
                $unit = new SithWarrior();
                break;

            case 2:
                $unit = new StormTrooper();
                break;

            default:
                throw new \Exception('Unsuporter Number');
                break;
        }
        return $unit;
    }
}