<?php
/**
 * Created by PhpStorm.
 * User: ivanhlevnjak
 * Date: 01/06/2018
 * Time: 20:51
 */

namespace App\Entity\Fleets;


class Fleet
{
    /** @var string */
    private $name;

    /** @var array */
    public $soliders;

    public function __construct()
    {
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @param array $soliders
     */
    public function setSoliders(array $soliders): void
    {
        $this->soliders = $soliders;
    }

    public function checkForSoliders()
    {
        if (count($this->soliders) > 0){
            return true;
        } else if (count($this->soliders)<= 0){
            return false;
        }
    }

    public function getRandomSolider(){
        return $this->soliders[array_rand($this->soliders)];
    }

    public function burySolider($solider)
    {
        $soliderIndex = array_search($solider, $this->soliders);
        unset($this->soliders[$soliderIndex]);
    }

    public function isAlive()
    {
        if(count($this->soliders)!= 0){
            return true;
        } else { return false; }
    }


}