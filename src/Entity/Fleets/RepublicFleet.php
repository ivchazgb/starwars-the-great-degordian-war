<?php
/**
 * Created by PhpStorm.
 * User: ivanhlevnjak
 * Date: 01/06/2018
 * Time: 20:57
 */

namespace App\Entity\Fleets;

use App\Entity\JediConsular;
use App\Entity\JediKnight;
use App\Entity\Solider;


class RepublicFleet extends Fleet
{
    public function __construct()
    {
        parent::__construct();
        $this->setName('Republic Fleet');
    }

    public function makeNewFleet(int $number): array
    {
        for ($i = 0; $i < $number; $i++) {

            $this->soliders[] = $this->getRandomRepublicClass();

        }
        return $this->soliders;
    }

    private function getRandomRepublicClass()
    {
        switch (rand(0, 2)) {
            case 0:
                $unit = new JediConsular();
                break;

            case 1:
                $unit = new JediKnight();
                break;

            case 2:
                $unit = new Solider();
                break;

            default:
                throw new \Exception('Unsuporter Number');
                break;
        }
        return $unit;
    }


}