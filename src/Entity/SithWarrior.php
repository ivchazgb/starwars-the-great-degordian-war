<?php

namespace App\Entity;

class SithWarrior extends Human
{

    /** @var string */
    private $type;


    public function __construct()
    {
        parent::__construct();
        $this->setType('SithWarrior');
        $this->setHealth($this->getHealth()+50);
        $this->setStrenght($this->getStrenght()+50);
        $this->setWeapon('Lightsaber');
        $this->setPerk('Double Lightsaber');
        $this->calculateDamageAndDefence();
    }
    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }


}
