<?php

namespace App\Entity;


class JediKnight extends Human
{

    private $type;


    public function __construct()
    {
        parent::__construct();
        $this->setType('JediKnight');
        $this->setHealth($this->getHealth()+60);
        $this->setStrenght($this->getStrenght()+30);
        $this->setWeapon('Lightsaber');
        $this->setPerk('R2D2');
        $this->calculateDamageAndDefence();
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }


}
