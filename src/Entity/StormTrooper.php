<?php

namespace App\Entity;


class StormTrooper extends Human
{
    /** @var string */
    private $type;


    public function __construct()
    {
        parent::__construct();
        $this->setType('StormTrooper');
        $this->setHealth($this->getHealth()+20);
        $this->setStrenght($this->getStrenght()+30);
        $this->setWeapon('PlasmaGun');
        $this->setPerk('JetPack');
        $this->calculateDamageAndDefence();
    }
    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

}
