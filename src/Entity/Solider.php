<?php

namespace App\Entity;

class Solider extends Human
{
    private $type;

    public function __construct()
    {
        parent::__construct();
        $this->setType('Solider');
        $this->setHealth($this->getHealth()+30);
        $this->setStrenght($this->getStrenght()+20);
        $this->setWeapon('LaserPistol');
        $this->setPerk('Comrade');
        $this->calculateDamageAndDefence();
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

}
