<?php

namespace App\Entity;

class JediConsular extends Human
{
    private $type;


    public function __construct()
    {
        parent::__construct();
        $this->setType('JediConsular');
        $this->setHealth($this->getHealth()+60);
        $this->setStrenght($this->getStrenght()+60);
        $this->setWeapon('DoubleBladedLightsaber');
        $this->setPerk('Apprentice');
        $this->calculateDamageAndDefence();
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }




}
