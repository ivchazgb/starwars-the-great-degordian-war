<?php

namespace App\Entity;


use App\Entity\Perks\Apprentice;
use App\Entity\Perks\Comrade;
use App\Entity\Perks\DestroyerShip;
use App\Entity\Perks\DoubleLightsaber;
use App\Entity\Perks\JetPack;
use App\Entity\Perks\R2D2;

class Human
{
    const BASE_HEALTH = 10;
    const BASE_STRENGHT = 10;

    /** @var integer */
    private $health;

    /** @var integer */
    private $strenght;

    /** @var string */
    private $weapon;

    /** @var integer */
    protected $damage;

    /** @var integer */
    protected $defence;

    /** @var string */
    protected $perk;

    public function __construct()
    {
        $this->health = self::BASE_HEALTH;
        $this->strenght = self::BASE_STRENGHT;

    }

    public function setWeapon(string $weapon): self
    {
        $this->weapon = $weapon;

        return $this;
    }

    public function getWeapon(): ?string
    {
        return $this->weapon;
    }

    public function getHealth(): ?int
    {
        return $this->health;
    }

    public function setHealth(int $health): self
    {
        $this->health = $health;

        return $this;
    }

    public function getStrenght(): ?int
    {
        return $this->strenght;
    }

    public function setStrenght(int $strenght): self
    {
        $this->strenght = $strenght;

        return $this;
    }

    /**
     * @return int
     */
    public function getDamage(): int
    {
        return $this->damage;
    }

    /**
     * @param int $damage
     */
    public function setDamage(int $damage): void
    {
        $this->damage = $damage;
    }

    /**
     * @return int
     */
    public function getDefence(): int
    {
        return $this->defence;
    }

    /**
     * @param int $defence
     */
    public function setDefence(int $defence): void
    {
        $this->defence = $defence;
    }

    /**
     * @return mixed
     */
    public function getPerk()
    {
        return $this->perk;
    }


    public function setPerk(string $perkName)
    {
        switch (rand(0, 1)){
            case 0:
                return $this->perk = $perkName;
            case 1:
                return $this->perk = null;
        }

    }

    public function calculateDamageAndDefence()
    {
        if ($this->perk != null) {
            $this->damage = $this->strenght + $this->getBonusDamage($this->perk);
            $this->defence = $this->health + $this->getBonusDefence($this->perk);
        } else {
            $this->damage = $this->strenght;
            $this->defence = $this->health;
        }
    }

    public function getBonusDamage($perkName)
    {
        switch ($perkName){
            case 'Apprentice':
                $apprentice = new Apprentice();
                return $apprentice->bonusDamage();
            case 'Comrade':
                $comrade = new Comrade();
                return $comrade->bonusDamage();
            case 'DestroyerShip':
                $destroyerShip = new DestroyerShip();
                return $destroyerShip->bonusDamage();
            case 'Double Lightsaber':
                $doubleLightsaber = new DoubleLightsaber();
                return $doubleLightsaber->bonusDamage();
            case 'JetPack':
                $jetPack = new JetPack();
                return $jetPack->bonusDamage();
            case 'R2D2':
                $r2D2 = new R2D2();
                return $r2D2->bonusDamage();
        }
    }

    public function getBonusDefence($perkName)
    {
        switch ($perkName){
            case 'Apprentice':
                $apprentice = new Apprentice();
                return $apprentice->bonusDefence();
            case 'Comrade':
                $comrade = new Comrade();
                return $comrade->bonusDefence();
            case 'DestroyerShip':
                $destroyerShip = new DestroyerShip();
                return $destroyerShip->bonusDefence();
            case 'Double Lightsaber':
                $doubleLightsaber = new DoubleLightsaber();
                return $doubleLightsaber->bonusDefence();
            case 'JetPack':
                $jetPack = new JetPack();
                return $jetPack->bonusDefence();
            case 'R2D2':
                $r2D2 = new R2D2();
                return $r2D2->bonusDefence();
        }
    }

    public function attack($defence)
    {
        $result = $defence - $this->damage;
        return $result;
    }

    public function isDead()
    {
        if ($this->defence > 0){
            return false;
        } else {
            return true;
        }
    }


}
