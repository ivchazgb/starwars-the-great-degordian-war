<?php

namespace App\Entity;




class SithLord extends Human
{
    /** @var string */
    private $type;


    public function __construct()
    {
        parent::__construct();
        $this->setType('SithLord');
        $this->setHealth($this->getHealth()+40);
        $this->setStrenght($this->getStrenght()+60);
        $this->setWeapon('DoubleBladedLightsaber');
        $this->setPerk('DestroyerShip');
        $this->calculateDamageAndDefence();
    }
    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

}
