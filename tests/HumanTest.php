<?php

namespace App\Tests;

use PHPUnit\Framework\TestCase;
use App\Entity\Human;

class HumanTest extends TestCase
{

    const STRENGTH = 10;

    public function testGetAndSetIsWorking()
    {
        $human = new Human();
        $human->setStrenght(self::STRENGTH);
        $this->assertSame(self::STRENGTH, $human->getStrenght());
    }

    public function testIsInstanceOfHuman()
    {
        $human = new Human();
        $this->assertInstanceOf(Human::class, $human);
    }


}
