<?php

namespace App\Tests;

use App\Entity\Human;
use App\Entity\Solider;
use PHPUnit\Framework\TestCase;

class SoliderTest extends TestCase
{
    const STRENGTH = 20;

    public function testIsInstanceOdHuman()
    {
        $solider = new Solider();
        $this->assertInstanceOf(Human::class, $solider);
    }

    public function testGetAndSetWorks()
    {
        $solider = new Solider();

        $solider->setStrenght(self::STRENGTH);

        $this->assertSame(self::STRENGTH, $solider->getStrenght());
    }
}
