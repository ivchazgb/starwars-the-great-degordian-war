<?php

namespace App\Tests;

use App\Entity\Human;
use App\Entity\SithLord;
use PHPUnit\Framework\TestCase;

class SithLordTest extends TestCase
{
    public function testIsInstanceOfHuman()
    {
        $sithLord = new SithLord();
        $this->assertInstanceOf(Human::class, $sithLord);
    }
}
