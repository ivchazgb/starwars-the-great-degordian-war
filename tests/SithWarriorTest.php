<?php

namespace App\Tests;

use App\Entity\Human;
use App\Entity\SithWarrior;
use PHPUnit\Framework\TestCase;

class SithWarriorTest extends TestCase
{
    public function testIsInstanceOfHuman()
    {
        $sithWarrior = new SithWarrior();
        $this->assertInstanceOf(Human::class, $sithWarrior);
    }
}
