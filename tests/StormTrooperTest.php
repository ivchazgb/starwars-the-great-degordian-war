<?php

namespace App\Tests;

use App\Entity\Human;
use App\Entity\StormTrooper;
use PHPUnit\Framework\TestCase;

class StormTrooperTest extends TestCase
{

    const STRENGTH = 20;

    /** @var StormTrooper */
    private $stormTrooper;

    public function setUp()
    {
        $this->stormTrooper = new StormTrooper();
    }

    public function testIsInstanceOdHuman()
    {

        $this->assertInstanceOf(Human::class, $this->stormTrooper);
    }

    public function testGetAndSetWorks()
    {

        $this->stormTrooper->setStrenght(self::STRENGTH);

        $this->assertSame(self::STRENGTH, $this->stormTrooper->getStrenght());
    }

    public function testGetAndSetHealthWorks()
    {
        $stormTrooperHealth = $this->stormTrooper->getHealth();
        $this->assertSame(Human::BASE_HEALTH + 20, 30);
    }
}
